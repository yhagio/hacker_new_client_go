package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const (
	apiBase     = "https://hacker-news.firebaseio.com/v0"
	topStories  = "/topstories"
	newStories  = "/newstories"
	bestStories = "/beststories"
	jobStories  = "/jobstories"
	askStories  = "/askstories"
	showStories = "/showstories"
	port        = 3000
	numStories  = 30
)

type itemWithHost struct {
	Item
	Host string
}

type templateData struct {
	Stories []itemWithHost
	Time    time.Duration
}

// Item: A single item returned by the HackerNews API.
// A type of "story", "comment", or "job" etc, and one of the
// URL or Text fields will be set, but not both.
type Item struct {
	By          string `json:"by"`
	Descendants int    `json:"descendants"`
	ID          int    `json:"id"`
	Parent      int    `json:"parent"`
	Kids        []int  `json:"kids"`
	Score       int    `json:"score"`
	Time        int    `json:"time"`
	Title       string `json:"title"`
	Type        string `json:"type"`

	// Only one of these should exist
	Text string `json:"text"`
	URL  string `json:"url"`
}

type Client struct {
	apiBase string
}

func (c *Client) defaultify() {
	if c.apiBase == "" {
		c.apiBase = apiBase
	}
}

func isStoryLink(item itemWithHost) bool {
	return item.Type == "story" && item.URL != ""
}

func parseHNItem(hnItem Item) itemWithHost {
	ret := itemWithHost{Item: hnItem}
	url, err := url.Parse(ret.URL)
	if err == nil {
		ret.Host = strings.TrimPrefix(url.Hostname(), "www.")
	}
	return ret
}

// Returns the ids of roughly ~450 top items in decreasing order.
func (c *Client) GetStories(storyType string) ([]int, error) {
	c.defaultify()
	resp, err := http.Get(fmt.Sprintf("%s%s.json", c.apiBase, storyType))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	var ids []int
	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&ids)
	if err != nil {
		return nil, err
	}

	return ids, nil
}

// Return the Item defined by the provided ID.
func (c *Client) GetItem(id int) (Item, error) {
	c.defaultify()
	var item Item
	resp, err := http.Get(fmt.Sprintf("%s/item/%d.json", c.apiBase, id))
	if err != nil {
		return item, err
	}

	defer resp.Body.Close()

	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&item)
	if err != nil {
		return item, err
	}

	return item, nil
}

func handler(numStories int, tpl *template.Template) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		var client Client

		ids, err := client.GetStories(topStories)
		if err != nil {
			http.Error(w, "Failed to get stories from Hacker News", http.StatusInternalServerError)
			return
		}

		var stories []itemWithHost
		for _, id := range ids {
			hnItem, err := client.GetItem(id)
			if err != nil {
				continue
			}

			item := parseHNItem(hnItem)
			if isStoryLink(item) {
				stories = append(stories, item)
				if len(stories) >= numStories {
					break
				}
			}
		}

		data := templateData{
			Stories: stories,
			Time:    time.Now().Sub(start),
		}

		err = tpl.Execute(w, data)
		if err != nil {
			http.Error(w, "Failed to execute the template", http.StatusInternalServerError)
			return
		}
	})
}

func main() {
	tpl := template.Must(template.ParseFiles("./index.gohtml"))
	http.HandleFunc("/", handler(numStories, tpl))
	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
